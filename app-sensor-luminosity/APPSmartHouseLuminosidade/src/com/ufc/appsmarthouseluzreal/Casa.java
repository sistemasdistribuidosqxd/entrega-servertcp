package com.ufc.appsmarthouseluzreal;

public class Casa {

	private int luminosidade; //sensorReal:
	private int ar;  //sensor2: moc
	private int luz; //sensor3: moc
	private int portao; //sensor4: moc
	private int porta; //sensor5: moc
	private int tv; //sensor6: moc
	
	
	public Casa(){
		
	}
	
	public int getLuminosidade() {
		return luminosidade;
	}
	public void setLuminosidade(int luminosidade) {
		this.luminosidade = luminosidade;
	}
	public int getAr() {
		return ar;
	}
	public void setAr(int ar) {
		this.ar = ar;
	}
	public int getLuz() {
		return luz;
	}
	public void setLuz(int luz) {
		this.luz = luz;
	}
	public int getPortao() {
		return portao;
	}
	public void setPortao(int portao) {
		this.portao = portao;
	}
	public int getPorta() {
		return porta;
	}
	public void setPorta(int porta) {
		this.porta = porta;
	}
	public int getTv() {
		return tv;
	}
	public void setTv(int tv) {
		this.tv = tv;
	}
	
	
}