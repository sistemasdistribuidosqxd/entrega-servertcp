package com.ufc.appsmarthouseluzreal;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;


import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.hardware.Sensor;

public class MainActivity extends Activity {

	private Switch luzes;

	private Casa casa;
	
	String flag;
	
	SensorObj sensor;
	
	Boolean sensorLigado;
	
	Boolean flagA;
	
	ServerSocket serverSocket;
	ServerSocket serverSocketLuminosidade;
	
	private SensorManager mSensorManager;
	private Sensor mLuz;
	private Sensor mProx;
	 
	TextView luzReal;
	 
	
	private TextView texttempo;
	
	private CountDownTimer tempo;
	
	public void onStart() {
		super.onStart();
		//outro
		new aguardandoConexaoLuminosidade().execute();
	
	}
	

	
	class ProxSensor implements SensorEventListener {
	 public void onAccuracyChanged(Sensor sensor, int accuracy) {
	         }
	 
	          public void onSensorChanged(SensorEvent event) {
	              float vl = event.values[0];
	              if (vl >= 10 ) {
	            	  
	               //   ivMicrofone.setImageResource(R.drawable.microfoneoff);
	              } else {
	                //  ivMicrofone.setImageResource(R.drawable.microfoneon);
	              }
	          }

			
	     }
	
	class LuzSensor implements SensorEventListener {
	        public void onAccuracyChanged(Sensor sensor, int accuracy) {
	          }
	         public void onSensorChanged(SensorEvent event) {
	            int vl = (int) event.values[0];
	             if (vl < 10) {
	            	 luzReal.setText(""+vl);
	            	 casa.setLuminosidade(vl);
	         //        llTela.setBackgroundColor(Color.WHITE);
	            } else {
	            	luzReal.setText(""+vl);
	            	casa.setLuminosidade(vl);
	          //       llTela.setBackgroundColor(Color.BLACK);
	             }
	         }
	     }
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        
        ///////
        texttempo = (TextView) findViewById(R.id.txt_tempo);
        luzReal = (TextView) findViewById(R.id.tvLuzReal);
        
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mLuz = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        mProx = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        mSensorManager.registerListener(new LuzSensor(), mLuz, SensorManager.SENSOR_DELAY_UI);
      //  mSensorManager.registerListener(new ProxSensor(), mProx, SensorManager.SENSOR_DELAY_UI);
        
        tempo = new CountDownTimer(30000, 1000) {
			
			@Override
			public void onTick(long arg0) {
				texttempo.setText(""+ arg0/1000);
				
			}
			
			@Override
			public void onFinish() {
				sensor=sensor.LUMINOSIDADE;
				texttempo.setText("Enviando novo Status");
				conectaServer();///
				tempo.cancel();
				tempo.start();
				sensor=sensor.PADRAO;
				
			}
		}.start();
        
  
        
        
        ///////
        
        sensorLigado=false;
        
        flagA=false;
        
        luzes = (Switch) findViewById(R.id.swLuz);

		sensor = sensor.PADRAO; //inicializando
		
		luzes.setChecked(false);
		
		casa = new Casa();
		casa.setLuz(0);
		
		luzes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
		    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		        if (isChecked && flagA==false) {
		        casa.setLuz(1);	
		        flag="LUZ";
		        sensor=sensor.LUZ;
		        conectaServer();
		        
		        } 
		        if(isChecked==false && flagA==false){
		        casa.setLuz(0);	
		        flag="LUZ";
		        sensor=sensor.LUZ;
		        conectaServer();
		        
		        	
		        }
		        flagA=false;
		    }
		});
		
	
		
		Button btSinc = (Button) findViewById(R.id.btSinc);
		btSinc.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
			conectaServer();
				
				
			}
			});
		
		
	}

	
void conectaServer(){
	Socket socket = null;
    DataOutputStream dataOutputStream = null;
	DataInputStream dataInputStream = null; 
   
	try{
    
//	socket = new Socket("192.168.0.63",8080);
//	socket = new Socket("192.168.15.3",8080);
//	socket = new Socket("192.168.43.84",8080); //ancoragem A7
	socket = new Socket("192.168.43.84",8080); //ancoragem A7
	
	dataOutputStream = new DataOutputStream(socket.getOutputStream());//dado que ser� enviado ao server
	dataInputStream = new DataInputStream(socket.getInputStream());  //dado que ser� recebido do server
	
	switch (sensor) {
	case LUZ:
		dataOutputStream.write(sensor.index());
		Toast.makeText(MainActivity.this, "Mandou LUZ: "+sensor.index()+" para o server", Toast.LENGTH_SHORT)
	    .show();
		
		dataOutputStream.write(casa.getLuz());
		
		int teste = dataInputStream.read();
		System.out.println("A luz esta: "+teste);
		Toast.makeText(MainActivity.this, "A luz esta: "+teste, Toast.LENGTH_SHORT)
	    .show();
		sensor=sensor.PADRAO;
		break;
		
	case AR:
		dataOutputStream.write(sensor.index());
		Toast.makeText(MainActivity.this, "Mandou AR: "+sensor.index()+" para o server", Toast.LENGTH_SHORT)
	    .show();
		
		dataOutputStream.write(casa.getAr());
		
		int teste2 = dataInputStream.read();
		System.out.println("O AR esta: "+teste2);
		Toast.makeText(MainActivity.this, "O AR esta: "+teste2, Toast.LENGTH_SHORT)
	    .show();
		sensor=sensor.PADRAO;
		break;
		
	case LUMINOSIDADE:
		dataOutputStream.write(sensor.index());
		Toast.makeText(MainActivity.this, "Mandou LUMINOSIDADE: "+sensor.index()+" para o server", Toast.LENGTH_SHORT)
	    .show();
		
		dataOutputStream.write(casa.getLuminosidade());
		
		int testeLuminosidade = dataInputStream.read();
		System.out.println("A luminosidade esta: "+testeLuminosidade);
		Toast.makeText(MainActivity.this, "A luminosidade esta: "+testeLuminosidade, Toast.LENGTH_SHORT)
	    .show();
		sensor=sensor.PADRAO;
		break;

	default:
		//sensor=sensor.PADRAO;
		dataOutputStream.write(sensor.index());
		Toast.makeText(MainActivity.this, "Mandou PADRAO: "+sensor.index()+" para o server", Toast.LENGTH_SHORT)
	    .show();
		
		int testeDefault = dataInputStream.read();
		System.out.println("A Luz esta: "+testeDefault);
		Toast.makeText(MainActivity.this, "A Luz esta: "+testeDefault, Toast.LENGTH_SHORT)
	    .show();
		if(testeDefault==1){
			flagA=true;
			ligarLed(this);
			luzes.setChecked(true);
		}else{
			flagA=true;
			desligarLed(this);
			luzes.setChecked(false);
		}
		
		
		int testeDefault2 = dataInputStream.read();
		int testeDefault3 = dataInputStream.read();
		int testeDefault4 = dataInputStream.read();
		int testeDefault5 = dataInputStream.read();
	//	System.out.println("O AR esta: "+testeDefault2);
	//	Toast.makeText(MainActivity.this, "O AR esta: "+testeDefault2, Toast.LENGTH_SHORT)
	//    .show();
		if(testeDefault2==1){
	//		ar.setChecked(true);
		}else{
	//		ar.setChecked(false);
		}
		
		
		break;
	}
	
	
	
	
	} catch (UnknownHostException e) {
		Toast.makeText(MainActivity.this, "Nenhuma Conex�o Detectada", Toast.LENGTH_SHORT)
        .show();
		e.printStackTrace();
		
	} catch (IOException e) {
		Toast.makeText(MainActivity.this, "Nenhuma Conex�o Detectada2", Toast.LENGTH_SHORT)
        .show();
		e.printStackTrace();
		
	} finally {
		
		try{
			if(socket!=null){
				socket.close();
			}
			if(dataOutputStream!=null){
				dataOutputStream.close();
			}
			if(dataInputStream!=null){
				dataInputStream.close();
			}
		} catch(Exception e){
			Toast.makeText(MainActivity.this, "Nenhuma Conex�o Detectada4", Toast.LENGTH_SHORT)
            .show();
			e.printStackTrace();
		}
		
	}
}

private Camera mCamera;

public void ligarLed(MainActivity mainActivity) {
    mCamera = Camera.open();
    if( mCamera != null ){
            Parameters params = mCamera.getParameters();
            params.setFlashMode( Parameters.FLASH_MODE_TORCH );
            mCamera.setParameters( params );
            mCamera.startPreview();
     }   

}

public void desligarLed(MainActivity mainActivity) {
    mCamera = Camera.open();
    if( mCamera != null ){
            Parameters params = mCamera.getParameters();
            params.setFlashMode( Parameters.FLASH_MODE_OFF );
            mCamera.setParameters( params );
            mCamera.startPreview();
     }   
   
}

private class aguardandoConexao extends AsyncTask<String, Void, Void>{

    private ProgressDialog dialog;
    protected void onPostExecute() {
    
    	dialog.cancel();
    	
    }

    @Override
    protected void onPreExecute() {
    	
    	Toast.makeText(MainActivity.this, "Sensor LUZ Ativo: Aguardando conex�es...", Toast.LENGTH_SHORT)
	    .show();
    }

    @Override
    protected Void doInBackground(final String... args) {
    	    	
    	try {
			serverSocket = new ServerSocket(8081);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Socket socket;
		
			
		while(true){
				
			try {
				socket = serverSocket.accept();
			
				DataOutputStream out = new DataOutputStream(socket.getOutputStream());
				DataInputStream in = new DataInputStream(socket.getInputStream());
				
				int ligado = in.read();
				
				if(ligado==casa.getLuz()){
			
				}
				if(ligado!=casa.getLuz()){
					casa.setLuz(ligado);
							
				}
				if(casa.getLuz()==1){
					ligarLed(MainActivity.this);
					
				}
				if(casa.getLuz()==0){
					desligarLed(MainActivity.this);
			
				}
				
				out.writeByte(casa.getLuz());
				
				out.flush();
				out.close();
				socket.close();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}             
		
		}

    }

}

private class aguardandoConexaoLuminosidade extends AsyncTask<String, Void, Void>{

    private ProgressDialog dialog;
    protected void onPostExecute() {
    
    	dialog.cancel();
    	
    }

    @Override
    protected void onPreExecute() {
    	
    	Toast.makeText(MainActivity.this, "Sensor LUMINOSIDADE Ativo: Aguardando conex�es...", Toast.LENGTH_SHORT)
	    .show();
    }

    @Override
    protected Void doInBackground(final String... args) {
    	    	
    	try {
			serverSocketLuminosidade = new ServerSocket(8086);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Socket socket;
		
			
		while(true){
				
			try {
				socket = serverSocketLuminosidade.accept();
			
				DataOutputStream out = new DataOutputStream(socket.getOutputStream());
				DataInputStream in = new DataInputStream(socket.getInputStream());
				
				int ligado = in.read();
				
				if(ligado==casa.getLuminosidade()){
			
				}
				if(ligado!=casa.getLuminosidade()){
					casa.setLuz(ligado);
							
				}
				if(casa.getLuminosidade()==1){
					//ligarLed(MainActivity.this);
					
				}
				if(casa.getLuminosidade()==0){
					//desligarLed(MainActivity.this);
			
				}
				
				out.writeByte(casa.getLuminosidade());
				
				out.flush();
				out.close();
				socket.close();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}             
		
		}

    }

}


}
