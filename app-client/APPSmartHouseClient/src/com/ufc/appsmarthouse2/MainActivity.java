package com.ufc.appsmarthouse2;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.Series;







import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	Switch luzes, ar, portao, porta, tv;
	
	Casa casa;
	
	String flag;
	
	//flags para resolver o caso do sincronizar mover o switch e n�o fazer uma nova conex�o com o server
	Boolean flagA;
	Boolean flagB;
	Boolean flagC;
	Boolean flagD;
	Boolean flagE;
	
	SensorObj sensor;
	
	ServerSocket serverSocketLuminosidade;

	TextView luzReal;

	private TextView texttempo;
	
	private CountDownTimer tempo;

	int x, y;
	
	GraphView graph;
	NumberFormat nf;
	LineGraphSeries<DataPoint> series;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
		
        flagA=false;
        flagB=false;
        flagC=false;
        flagD=false;
        flagE=false;
        
        luzes = (Switch) findViewById(R.id.swLuz);
		ar = (Switch) findViewById(R.id.swAr);
		portao = (Switch) findViewById(R.id.swPortao);
		porta = (Switch) findViewById(R.id.swPorta);
		tv = (Switch) findViewById(R.id.swTv);
		
		texttempo = (TextView) findViewById(R.id.txt_tempo);
	    luzReal = (TextView) findViewById(R.id.tvLuzReal);
		
		sensor = sensor.PADRAO; //inicializando
		
		luzes.setChecked(false);
		
		casa = new Casa();
		casa.setLuminosidade(1);
		x=0;
		y=0;
		graph = (GraphView) findViewById(R.id.graph);
		
	//	nf = NumberFormat.getInstance();
	//	nf.setMinimumFractionDigits(2);
	//	nf.setMinimumIntegerDigits(2);
		
		tempo = new CountDownTimer(30000, 1000) {
			
			@Override
			public void onTick(long arg0) {
				texttempo.setText(""+ arg0/1000);
				
			}
			
			@Override
			public void onFinish() {
				sensor=sensor.LUMINOSIDADE;
				texttempo.setText("Recebendo novo Status");
				conectaServer();///
				
				y=casa.getLuminosidade();
				if(x>8){
					x=0;
					
				}
				
				series = new LineGraphSeries<DataPoint>(new DataPoint[]{
					new DataPoint(x, y),
					
				
				});
				
			//	graph.addSeries((Series) a1);
				
		//		graph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter(nf,nf));
				series.setTitle("Curva de Mudan�as na Luminosidade");
				series.setColor(Color.BLUE);
				series.setDrawDataPoints(true);
				series.setDataPointsRadius(10);
				series.setThickness(8);
				graph.addSeries(series);
				
				x++;
				//tempo.cancel();
				tempo.start();
				sensor=sensor.PADRAO;
				
			}
		}.start();
		
		///
		
		
		
		
		
		////
		luzes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
		    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		        if (isChecked && flagA==false) {
		        casa.setLuz(1);	
		        flag="LUZ";
		        sensor=sensor.LUZ;
		        conectaServer();
		        
		        } 
		        if(isChecked==false && flagA==false){
		        casa.setLuz(0);	
		        flag="LUZ";
		        sensor=sensor.LUZ;
		        conectaServer();
		        
		        	
		        }
		        flagA=false;
		    }
		});
		
		ar.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
		    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		        if (isChecked && flagB==false) {
		        casa.setAr(1);	
		        flag="AR";
		        sensor=sensor.AR;
		        conectaServer();
		        
		        } if(isChecked==false && flagB==false){
		        casa.setAr(0);	
		        flag="AR";
		        sensor=sensor.AR;
		        conectaServer();
		        
		        	
		        }
		        flagB=false;
		    }
		});
		
		portao.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
		    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		        if (isChecked && flagC==false) {
		        casa.setPortao(1);	
		        flag="PORTAO";
		        sensor=sensor.PORTAO;
		        conectaServer();
		        
		        } if(isChecked==false && flagC==false){
		        casa.setPortao(0);	
		        flag="Portao";
		        sensor=sensor.PORTAO;
		        conectaServer();
		        
		        	
		        }
		        flagC=false;
		    }
		});
		
		porta.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
		    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		        if (isChecked && flagE==false) {
		        casa.setPorta(1);	
		        flag="PORTA";
		        sensor=sensor.PORTA;
		        conectaServer();
		        
		        } if(isChecked==false && flagE==false){
		        casa.setPorta(0);	
		        flag="PORTA";
		        sensor=sensor.PORTA;
		        conectaServer();
		        
		        	
		        }
		        flagE=false;
		    }
		});
		
		tv.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
		    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		        if (isChecked && flagD==false) {
		        casa.setTv(1);	
		        flag="TV";
		        sensor=sensor.TV;
		        conectaServer();
		        
		        } if(isChecked==false && flagD==false){
		        casa.setTv(0);	
		        flag="TV";
		        sensor=sensor.TV;
		        conectaServer();
		        
		        	
		        }
		        flagD=false;
		    }
		});
		
		
		
		Button btSinc = (Button) findViewById(R.id.btSinc);
		btSinc.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
			conectaServer();
				
				
			}
			});
		
		
	}

	
void conectaServer(){
	Socket socket = null;
    DataOutputStream dataOutputStream = null;
	DataInputStream dataInputStream = null; 
   
	try{
    
//	socket = new Socket("192.168.0.63",8080);
//	socket = new Socket("192.168.15.3",8080);
	socket = new Socket("192.168.43.84",8080); //ancoragem A7
		
	dataOutputStream = new DataOutputStream(socket.getOutputStream());//dado que ser� enviado ao server
	dataInputStream = new DataInputStream(socket.getInputStream());  //dado que ser� recebido do server
	
	switch (sensor) {
	case LUZ:
		dataOutputStream.write(sensor.index());
		Toast.makeText(MainActivity.this, "Mandou LUZ: "+sensor.index()+" para o server", Toast.LENGTH_SHORT)
	    .show();
		
		dataOutputStream.write(casa.getLuz());
		
		int teste = dataInputStream.read();
		
		if(teste!=0 && teste!=1){
		Toast.makeText(MainActivity.this, "O sensor est� desconectado "+teste, Toast.LENGTH_SHORT)
		    .show();
		//luzes.setChecked();
		}
		if(teste==0 || teste==1){
		System.out.println("A luz esta: "+teste);
		Toast.makeText(MainActivity.this, "A luz esta: "+teste, Toast.LENGTH_SHORT)
		    .show();	
		
		
		}
		
		sensor=sensor.PADRAO;
		break;
		
	case AR:
		dataOutputStream.write(sensor.index());
		Toast.makeText(MainActivity.this, "Mandou AR: "+sensor.index()+" para o server", Toast.LENGTH_SHORT)
	    .show();
		
		dataOutputStream.write(casa.getAr());
		
		int testeAr = dataInputStream.read();
		
		if(testeAr!=0 && testeAr!=1){
			Toast.makeText(MainActivity.this, "O sensor AR est� desconectado "+testeAr, Toast.LENGTH_SHORT)
			    .show();
			
			}
			if(testeAr==0 || testeAr==1){
			System.out.println("O Ar esta: "+testeAr);
			Toast.makeText(MainActivity.this, "O Ar esta: "+testeAr, Toast.LENGTH_SHORT)
			    .show();	
			
			
			}
		sensor=sensor.PADRAO;
		break;
		
	case PORTAO:
		dataOutputStream.write(sensor.index());
		Toast.makeText(MainActivity.this, "Mandou PORTAO: "+sensor.index()+" para o server", Toast.LENGTH_SHORT)
	    .show();
		
		dataOutputStream.write(casa.getPortao());
		
		int testePortao = dataInputStream.read();
		
		if(testePortao!=0 && testePortao!=1){
		Toast.makeText(MainActivity.this, "O sensor PORTAO est� desconectado "+testePortao, Toast.LENGTH_SHORT)
		    .show();
		
		}
		if(testePortao==0 || testePortao==1){
		System.out.println("O Portao esta: "+testePortao);
		Toast.makeText(MainActivity.this, "O Portao esta: "+testePortao, Toast.LENGTH_SHORT)
		    .show();	
		
		
		}
		sensor=sensor.PADRAO;
		break;
		
	case PORTA:
		dataOutputStream.write(sensor.index());
		Toast.makeText(MainActivity.this, "Mandou PORTA: "+sensor.index()+" para o server", Toast.LENGTH_SHORT)
	    .show();
		
		dataOutputStream.write(casa.getPorta());
		
		int testePorta = dataInputStream.read();
		
		if(testePorta!=0 && testePorta!=1){
		Toast.makeText(MainActivity.this, "O sensor PORTA est� desconectado "+testePorta, Toast.LENGTH_SHORT)
		    .show();
		
		}
		if(testePorta==0 || testePorta==1){
		System.out.println("A Porta esta: "+testePorta);
		Toast.makeText(MainActivity.this, "A Porta esta: "+testePorta, Toast.LENGTH_SHORT)
		    .show();	
		
		
		}
		sensor=sensor.PADRAO;
		break;
		
	case TV:
		dataOutputStream.write(sensor.index());
		Toast.makeText(MainActivity.this, "Mandou TV: "+sensor.index()+" para o server", Toast.LENGTH_SHORT)
	    .show();
		
		dataOutputStream.write(casa.getTv());
		
		int testeTv = dataInputStream.read();
		
		if(testeTv!=0 && testeTv!=1){
		Toast.makeText(MainActivity.this, "O sensor TV est� desconectado "+testeTv, Toast.LENGTH_SHORT)
		    .show();
		
		}
		if(testeTv==0 || testeTv==1){
		System.out.println("A Tv esta: "+testeTv);
		Toast.makeText(MainActivity.this, "A Tv esta: "+testeTv, Toast.LENGTH_SHORT)
		    .show();	
		
		
		}
		sensor=sensor.PADRAO;
		break;
		
	case LUMINOSIDADE:
		dataOutputStream.write(sensor.index());
		Toast.makeText(MainActivity.this, "Mandou LUMINOSIDADE: "+sensor.index()+" para o server", Toast.LENGTH_SHORT)
	    .show();
		
		dataOutputStream.write(casa.getLuminosidade());
		
		int testeLuminosidade = dataInputStream.read();
		System.out.println("A luminosidade esta: "+testeLuminosidade);
		Toast.makeText(MainActivity.this, "A luminosidade esta: "+testeLuminosidade, Toast.LENGTH_SHORT)
	    .show();
		 if(testeLuminosidade==255){
    		 luzReal.setText("Desconectado: "+testeLuminosidade);
    		 //casa.setLuminosidade(0);
    	 }else{
    		 luzReal.setText(""+testeLuminosidade);
    		 casa.setLuminosidade(testeLuminosidade);
    	 }
		//casa.setLuminosidade(testeLuminosidade);
		sensor=sensor.PADRAO;
		break;

	default:
		//sensor=sensor.PADRAO;
		dataOutputStream.write(sensor.index());
		Toast.makeText(MainActivity.this, "Mandou PADRAO: "+sensor.index()+" para o server", Toast.LENGTH_SHORT)
	    .show();
		
		int testeDefault = dataInputStream.read();
		System.out.println("A Luz esta: "+testeDefault);
		Toast.makeText(MainActivity.this, "A Luz esta: "+testeDefault, Toast.LENGTH_SHORT)
	    .show();
	//	casa.setLuz(testeDefault); add flaA
		if(testeDefault==1){
			flagA=true;
			luzes.setChecked(true);
		}else{
			flagA=true;
			luzes.setChecked(false);
		}
		
		
		int testeDefault2 = dataInputStream.read();
		System.out.println("O AR esta: "+testeDefault2);
		Toast.makeText(MainActivity.this, "O AR esta: "+testeDefault2, Toast.LENGTH_SHORT)
	    .show();
		if(testeDefault2==1){
			flagB=true;
			ar.setChecked(true);
		}else{
			flagB=true;
			ar.setChecked(false);
		}
		
		
		int testeDefault3 = dataInputStream.read();
		System.out.println("O PORTAO esta: "+testeDefault3);
		Toast.makeText(MainActivity.this, "O Portao esta: "+testeDefault3, Toast.LENGTH_SHORT)
	    .show();
		if(testeDefault3==1){
			flagC=true;
			portao.setChecked(true);
		}else{
			flagC=true;
			portao.setChecked(false);
		}
		
		int testeDefault4 = dataInputStream.read();
		System.out.println("A PORTA esta: "+testeDefault4);
		Toast.makeText(MainActivity.this, "A Porta esta: "+testeDefault4, Toast.LENGTH_SHORT)
	    .show();
		if(testeDefault4==1){
			flagE=true;
			porta.setChecked(true);
		}else{
			flagE=true;
			porta.setChecked(false);
		}
		
		int testeDefault5 = dataInputStream.read();
		System.out.println("A TV esta: "+testeDefault5);
		Toast.makeText(MainActivity.this, "A Tv esta: "+testeDefault5, Toast.LENGTH_SHORT)
	    .show();
		if(testeDefault5==1){
			flagD=true;
			tv.setChecked(true);
		}else{
			flagD=true;
			tv.setChecked(false);
		}
		
		break;
	}
	
	
	
	
	} catch (UnknownHostException e) {
		Toast.makeText(MainActivity.this, "Nenhuma Conex�o Detectada", Toast.LENGTH_SHORT)
        .show();
		e.printStackTrace();
		
	} catch (IOException e) {
		Toast.makeText(MainActivity.this, "Nenhuma Conex�o Detectada2", Toast.LENGTH_SHORT)
        .show();
		e.printStackTrace();
		
	} finally {
		
		try{
			if(socket!=null){
				socket.close();
			}
			if(dataOutputStream!=null){
				dataOutputStream.close();
			}
			if(dataInputStream!=null){
				dataInputStream.close();
			}
		} catch(Exception e){
			Toast.makeText(MainActivity.this, "Nenhuma Conex�o Detectada3", Toast.LENGTH_SHORT)
            .show();
			e.printStackTrace();
		}
		
	}
}


}
