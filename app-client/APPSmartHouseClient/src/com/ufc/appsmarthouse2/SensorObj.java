package com.ufc.appsmarthouse2;

public enum SensorObj {
    LUZ (0), AR (1), PORTAO(2), TV(3), PORTA(4), PADRAO(5), LUMINOSIDADE(6);
    
    private final int index;   

    SensorObj(int index) {
        this.index = index;
    }

    public int index() { 
        return index; 
    }
	
}
