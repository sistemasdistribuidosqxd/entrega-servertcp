package com.ufc.appsmarthouseportao;

public enum Sensor {
    LUZ (0), AR (1), PORTAO(2), TV(3), PORTA(4), PADRAO(5);
    
    private final int index;   

    Sensor(int index) {
        this.index = index;
    }

    public int index() { 
        return index; 
    }
	
}
