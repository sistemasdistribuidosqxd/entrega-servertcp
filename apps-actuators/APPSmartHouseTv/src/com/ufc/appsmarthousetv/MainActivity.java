package com.ufc.appsmarthousetv;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;


import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

public class MainActivity extends Activity {

	private Switch tv;

	private Casa casa;
	
	String flag;
	
	Sensor sensor;
	
	Boolean sensorLigado;
	
	Boolean flagA;
	
	ServerSocket serverSocket;
	
	
	public void onStart() {
		super.onStart();
		
		new aguardandoConexao().execute();
	
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        
        sensorLigado=false;
        
        flagA=false;
        
        tv = (Switch) findViewById(R.id.swTv);

		sensor = sensor.PADRAO; //inicializando
		
		tv.setChecked(false);
		
		casa = new Casa();
		casa.setTv(0);
		
		tv.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
		    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		        if (isChecked && flagA==false) {
		        casa.setTv(1);	
		        flag="TV";
		        sensor=sensor.TV;
		        conectaServer();
		        
		        } 
		        if(isChecked==false && flagA==false) {
		        casa.setTv(0);	
		        flag="TV";
		        sensor=sensor.TV;
		        conectaServer();
		        
		        	
		        }
		        flagA=false;
		    }
		});
		
	
		
		Button btSinc = (Button) findViewById(R.id.btSinc);
		btSinc.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
			conectaServer();
				
				
			}
			});
		
		
	}

	
void conectaServer(){
	Socket socket = null;
    DataOutputStream dataOutputStream = null;
	DataInputStream dataInputStream = null; 
   
	try{
    
//	socket = new Socket("192.168.0.63",8080);
	socket = new Socket("192.168.43.84",8080); //ancoragem A7
	
	dataOutputStream = new DataOutputStream(socket.getOutputStream());//dado que ser� enviado ao server
	dataInputStream = new DataInputStream(socket.getInputStream());  //dado que ser� recebido do server
	
	switch (sensor) {
	case LUZ:
		dataOutputStream.write(sensor.index());
		Toast.makeText(MainActivity.this, "Mandou LUZ: "+sensor.index()+" para o server", Toast.LENGTH_SHORT)
	    .show();
		
		dataOutputStream.write(casa.getLuz());
		
		int teste = dataInputStream.read();
		System.out.println("A luz esta: "+teste);
		Toast.makeText(MainActivity.this, "A luz esta: "+teste, Toast.LENGTH_SHORT)
	    .show();
		sensor=sensor.PADRAO;
		break;
		
	case AR:
		dataOutputStream.write(sensor.index());
		Toast.makeText(MainActivity.this, "Mandou AR: "+sensor.index()+" para o server", Toast.LENGTH_SHORT)
	    .show();
		
		dataOutputStream.write(casa.getAr());
		
		int teste2 = dataInputStream.read();
		System.out.println("O AR esta: "+teste2);
		Toast.makeText(MainActivity.this, "O AR esta: "+teste2, Toast.LENGTH_SHORT)
	    .show();
		sensor=sensor.PADRAO;
		break;
		
	case PORTAO:
		dataOutputStream.write(sensor.index());
		Toast.makeText(MainActivity.this, "Mandou PORTAO: "+sensor.index()+" para o server", Toast.LENGTH_SHORT)
	    .show();
		
		dataOutputStream.write(casa.getPortao());
		
		int teste3 = dataInputStream.read();
		System.out.println("O PORTAO esta: "+teste3);
		Toast.makeText(MainActivity.this, "O PORTAO esta: "+teste3, Toast.LENGTH_SHORT)
	    .show();
		sensor=sensor.PADRAO;
		break;
	
	case PORTA:
		dataOutputStream.write(sensor.index());
		Toast.makeText(MainActivity.this, "Mandou PORTA: "+sensor.index()+" para o server", Toast.LENGTH_SHORT)
	    .show();
		
		dataOutputStream.write(casa.getPorta());
		
		int teste4 = dataInputStream.read();
		System.out.println("A PORTA esta: "+teste4);
		Toast.makeText(MainActivity.this, "A PORTA esta: "+teste4, Toast.LENGTH_SHORT)
	    .show();
		sensor=sensor.PADRAO;
		break;
		
	case TV:
		dataOutputStream.write(sensor.index());
		Toast.makeText(MainActivity.this, "Mandou TV: "+sensor.index()+" para o server", Toast.LENGTH_SHORT)
	    .show();
		
		dataOutputStream.write(casa.getTv());
		
		int teste5 = dataInputStream.read();
		System.out.println("A TV esta: "+teste5);
		Toast.makeText(MainActivity.this, "A TV esta: "+teste5, Toast.LENGTH_SHORT)
	    .show();
		sensor=sensor.PADRAO;
		break;

	default:
		//sensor=sensor.PADRAO;
		dataOutputStream.write(sensor.index());
		Toast.makeText(MainActivity.this, "Mandou PADRAO: "+sensor.index()+" para o server", Toast.LENGTH_SHORT)
	    .show();
		
		int testeDefault = dataInputStream.read();
	//	System.out.println("A Luz esta: "+testeDefault);
	//	Toast.makeText(MainActivity.this, "O Luz esta: "+testeDefault, Toast.LENGTH_SHORT)
	//    .show();
		if(testeDefault==1){
	//		luzes.setChecked(true);
		}else{
	//		luzes.setChecked(false);
		}
		
		
		int testeDefault2 = dataInputStream.read();
//		System.out.println("O AR esta: "+testeDefault2);
//		Toast.makeText(MainActivity.this, "O AR esta: "+testeDefault2, Toast.LENGTH_SHORT)
//	    .show();
		if(testeDefault2==1){
//			ar.setChecked(true);
		}else{
//			ar.setChecked(false);
		}
		
		
		int testeDefault3 = dataInputStream.read();
//		System.out.println("O PORTAO esta: "+testeDefault3);
//		Toast.makeText(MainActivity.this, "O Portao esta: "+testeDefault3, Toast.LENGTH_SHORT)
///	    .show();
		if(testeDefault3==1){
//			portao.setChecked(true);
		}else{
//			portao.setChecked(false);
		}
		
		int testeDefault4 = dataInputStream.read();
	//	System.out.println("A PORTA esta: "+testeDefault4);
	//	Toast.makeText(MainActivity.this, "A Porta esta: "+testeDefault4, Toast.LENGTH_SHORT)
	 //   .show();
		if(testeDefault4==1){
	//		porta.setChecked(true);
		}else{
	//		porta.setChecked(false);
		}
		
		int testeDefault5 = dataInputStream.read();
		System.out.println("A TV esta: "+testeDefault5);
		Toast.makeText(MainActivity.this, "A Tv esta: "+testeDefault5, Toast.LENGTH_SHORT)
	   .show();
		if(testeDefault5==1){
			flagA=true;
			ligarLed(this);
			tv.setChecked(true);
		}else{
			flagA=true;
			desligarLed(this);
			tv.setChecked(false);
		}
	
		break;
	}
	
	
	
	
	} catch (UnknownHostException e) {
		Toast.makeText(MainActivity.this, "Nenhuma Conex�o Detectada", Toast.LENGTH_SHORT)
        .show();
		e.printStackTrace();
		
	} catch (IOException e) {
		Toast.makeText(MainActivity.this, "Nenhuma Conex�o Detectada2", Toast.LENGTH_SHORT)
        .show();
		e.printStackTrace();
		
	} finally {
		
		try{
			if(socket!=null){
				socket.close();
			}
			if(dataOutputStream!=null){
				dataOutputStream.close();
			}
			if(dataInputStream!=null){
				dataInputStream.close();
			}
		} catch(Exception e){
			Toast.makeText(MainActivity.this, "Nenhuma Conex�o Detectada3", Toast.LENGTH_SHORT)
            .show();
			e.printStackTrace();
		}
		
	}
}

private Camera mCamera;

public void ligarLed(MainActivity mainActivity) {
    mCamera = Camera.open();
    if( mCamera != null ){
            Parameters params = mCamera.getParameters();
            params.setFlashMode( Parameters.FLASH_MODE_TORCH );
            mCamera.setParameters( params );
            mCamera.startPreview();
     }   

}

public void desligarLed(MainActivity mainActivity) {
    mCamera = Camera.open();
    if( mCamera != null ){
            Parameters params = mCamera.getParameters();
            params.setFlashMode( Parameters.FLASH_MODE_OFF );
            mCamera.setParameters( params );
            mCamera.startPreview();
     }   
   
}

private class aguardandoConexao extends AsyncTask<String, Void, Void>{

    private ProgressDialog dialog;
    protected void onPostExecute() {
    
    	dialog.cancel();
    	
    }

    @Override
    protected void onPreExecute() {
    	
    	Toast.makeText(MainActivity.this, "Sensor TV Ativo: Aguardando conex�es...", Toast.LENGTH_SHORT)
	    .show();
    }

    @Override
    protected Void doInBackground(final String... args) {
    	    	
    	try {
			serverSocket = new ServerSocket(8084); //8084 TV
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Socket socket = null;
		
			
		while(true){
				
			try {
				socket = serverSocket.accept();
			
				DataOutputStream out = new DataOutputStream(socket.getOutputStream());
				DataInputStream in = new DataInputStream(socket.getInputStream());
				
				int ligado = in.read();
				
				if(ligado==casa.getTv()){
			
				}
				if(ligado!=casa.getTv()){
					casa.setTv(ligado);
							
				}
				if(casa.getTv()==1){
					ligarLed(MainActivity.this);
					
				}
				if(casa.getTv()==0){
					desligarLed(MainActivity.this);
			
				}
				
				out.writeByte(casa.getTv());
				
				
				out.flush();
				out.close();
				socket.close();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}             
		
		}

    }

}


}
