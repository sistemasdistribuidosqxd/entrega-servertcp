package com.ufc.appsmarthouseserver;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;


public class MainServer {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public static void main(String[] args) throws IOException, ClassNotFoundException {
	
		ServerSocket serverSocket = new ServerSocket(8080);
		Socket socket;
		
		Casa casa = new Casa();
		String flag;
		Sensor sensor;
		
		while(true){
			System.out.println("Aguardando conex�es... ...");
			
			socket = serverSocket.accept();                //para a aplicacao cliente
			System.out.println("Conectado com cliente!!!");
			
			DataOutputStream out = new DataOutputStream(socket.getOutputStream());
			DataInputStream in = new DataInputStream(socket.getInputStream());
			
			//sensor.index() = in.read();
			int aux = in.read();
			switch (aux) {
			case 0://LUZ
				System.out.println("***O servidor recebeu LUZ:"+ aux + "***");
				int confirmacao = conectaSensorLuz(in.read());
				if(confirmacao==1 || confirmacao==0){
					casa.setLuz(confirmacao);
					System.out.println("A luz esta: "+casa.getLuz());
					out.writeByte(casa.getLuz());
				}
				if(confirmacao==-1){
					System.out.println("O sensor LUZ est� desconectado: "+confirmacao);
					out.writeByte(confirmacao);
				}
			 
				break;
				
			case 1://AR
				System.out.println("***O servidor recebeu AR:"+ aux + "***");
				int confirmacaoAr = conectaSensorAr(in.read());
				if(confirmacaoAr==1 || confirmacaoAr==0){
					casa.setAr(confirmacaoAr);
					System.out.println("O Ar esta: "+casa.getAr());
					out.writeByte(casa.getAr());
				}
				if(confirmacaoAr==-1){
					System.out.println("O sensor AR est� desconectado: "+confirmacaoAr);
					out.writeByte(confirmacaoAr);
				}
				
				break;
				
			case 2://PORTAO
				System.out.println("***O servidor recebeu PORTAO:"+ aux + "***");
				int confirmacaoPortao = conectaSensorPortao(in.read());
				if(confirmacaoPortao==1 || confirmacaoPortao==0){
					casa.setPortao(confirmacaoPortao);
					System.out.println("O Portao esta: "+casa.getPortao());
					out.writeByte(casa.getPortao());
				}
				if(confirmacaoPortao==-1){
					System.out.println("O sensor PORTAO est� desconectado: "+confirmacaoPortao);
					out.writeByte(confirmacaoPortao);
				}
				
				break;
				
			case 3://TV
				System.out.println("***O servidor recebeu TV:"+ aux + "***");
				int confirmacaoTv = conectaSensorTv(in.read());
				if(confirmacaoTv==1 || confirmacaoTv==0){
					casa.setTv(confirmacaoTv);
					System.out.println("A Tv esta: "+casa.getTv());
					out.writeByte(casa.getTv());
				}
				if(confirmacaoTv==-1){
					System.out.println("O sensor TV est� desconectado: "+confirmacaoTv);
					out.writeByte(confirmacaoTv);
				}
				
				break;
				
			case 4://PORTA
				System.out.println("***O servidor recebeu PORTA:"+ aux + "***");
				int confirmacaoPorta = conectaSensorPorta(in.read());
				if(confirmacaoPorta==1 || confirmacaoPorta==0){
					casa.setPorta(confirmacaoPorta);
					System.out.println("A Porta esta: "+casa.getPorta());
					out.writeByte(casa.getPorta());
				}
				if(confirmacaoPorta==-1){
					System.out.println("O sensor PORTA est� desconectado: "+confirmacaoPorta);
					out.writeByte(confirmacaoPorta);
				}
			
				
				break;
			
			case 6://LUMINOSIDADE
				System.out.println("***O servidor recebeu LUMINOSIDADE:"+ aux + "***");
				int confirmacaoLuminosidade = conectaSensorLuminosidade(in.read());
				if(confirmacaoLuminosidade!=-1){
					casa.setLuminosidade(confirmacaoLuminosidade);
					System.out.println("A Luminosidade esta: "+casa.getLuminosidade());
					out.writeByte(casa.getLuminosidade());
				}
				if(confirmacaoLuminosidade==-1){
					System.out.println("O sensor LUMINOSIDADE est� desconectado: "+confirmacaoLuminosidade);
					out.writeByte(confirmacaoLuminosidade);
				}
			
				
				break;

			default:
				System.out.println("***O servidor recebeu PADRAO:"+ aux + "***");
				System.out.println("Requisi��o de Sincronia");
				out.writeByte(casa.getLuz());
				out.writeByte(casa.getAr());
				out.writeByte(casa.getPortao());
				out.writeByte(casa.getPorta());
				out.writeByte(casa.getTv());
				out.writeByte(casa.getLuminosidade());
				
				break;
			}
			
			
						
			
															//enviando objeto de volta ao cliente
			
			out.flush();
			
			System.out.println("Mensagem Enviada!!!");
			System.out.println("... ...Fechando Conex�o");
			
			System.out.println("\n***Status da SmartHome***");
			System.out.println("***Luzes:"+ casa.getLuz() + "***");
			System.out.println("***Ar Condicionado:"+ casa.getAr() + "***");
			System.out.println("***Portao:"+ casa.getPortao() + "***");
			System.out.println("***Tv:"+ casa.getTv() + "***");
			System.out.println("***Porta:"+ casa.getPorta() + "***\n\n");
			System.out.println("***Luminosidade:"+ casa.getLuminosidade() + "***\n\n");
			
			
			
			
			out.close();
			socket.close();
			
			
			
			
			
			
		}

	}
	

static int conectaSensorLuz(int ligado){
	int teste=-1;
	Socket socket = null;
    DataOutputStream dataOutputStream = null;
	DataInputStream dataInputStream = null; 
   
	try{
    
//	socket = new Socket("192.168.0.25",8081); // luz
//	socket = new Socket("192.168.15.6",8081); // luz
//	socket = new Socket("192.168.43.33",8081); // luz
	socket = new Socket("192.168.43.204",8081);
	
	dataOutputStream = new DataOutputStream(socket.getOutputStream());//dado que ser� enviado ao server
	dataInputStream = new DataInputStream(socket.getInputStream());  //dado que ser� recebido do server
	
	
	dataOutputStream.write(ligado); //mandando a informa��o para ligar o sensor
	
		
//	dataOutputStream.write(casa.getLuz());
		
	teste = dataInputStream.read();  //recebendo a confirma��o se ligou 
	System.out.println("O sensor Luz enviou: "+teste);
	
	} catch (UnknownHostException e) {
	System.out.println("Nenhuma Conex�o Detectada1");
		e.printStackTrace();
		
	} catch (IOException e) {
		//teste=-1;
		System.out.println("Nenhuma Conex�o Detectada2");
		e.printStackTrace();
		return -1;
		
		
		
	} finally {
		
		try{
			if(socket!=null){
				socket.close();
			}
			if(dataOutputStream!=null){
				dataOutputStream.close();
			}
			if(dataInputStream!=null){
				dataInputStream.close();
			}
		} catch(Exception e){
			System.out.println("Nenhuma Conex�o Detectada3");
			e.printStackTrace();
		}
		
	}
	return teste;
}

static int conectaSensorAr(int ligado){
	int teste=-1;
	Socket socket = null;
    DataOutputStream dataOutputStream = null;
	DataInputStream dataInputStream = null; 
   
	try{
    
//	socket = new Socket("192.168.0.25",8082); // Ar
//	socket = new Socket("192.168.15.6",8082);
//	socket = new Socket("192.168.43.33",8082);
	socket = new Socket("192.168.43.204",8082);
		
	dataOutputStream = new DataOutputStream(socket.getOutputStream());//dado que ser� enviado ao server
	dataInputStream = new DataInputStream(socket.getInputStream());  //dado que ser� recebido do server
	
	
	dataOutputStream.write(ligado); //mandando a informa��o para ligar o sensor
	
	teste = dataInputStream.read();  //recebendo a confirma��o se ligou 
	System.out.println("O sensor Ar enviou: "+teste);
	
	} catch (UnknownHostException e) {
	System.out.println("Nenhuma Conex�o Detectada1");
		e.printStackTrace();
		
	} catch (IOException e) {
		//teste=-1;
		System.out.println("Nenhuma Conex�o Detectada2");
		e.printStackTrace();
		return -1;
		
		
		
	} finally {
		
		try{
			if(socket!=null){
				socket.close();
			}
			if(dataOutputStream!=null){
				dataOutputStream.close();
			}
			if(dataInputStream!=null){
				dataInputStream.close();
			}
		} catch(Exception e){
			System.out.println("Nenhuma Conex�o Detectada3");
			e.printStackTrace();
		}
		
	}
	return teste;
}

static int conectaSensorPortao(int ligado){
	int teste=-1;
	Socket socket = null;
    DataOutputStream dataOutputStream = null;
	DataInputStream dataInputStream = null; 
   
	try{
    
//	socket = new Socket("192.168.0.25",8083); // Portao
//	socket = new Socket("192.168.43.33",8083);
	socket = new Socket("192.168.43.204",8083);
	
	dataOutputStream = new DataOutputStream(socket.getOutputStream());//dado que ser� enviado ao server
	dataInputStream = new DataInputStream(socket.getInputStream());  //dado que ser� recebido do server
	
	
	dataOutputStream.write(ligado); //mandando a informa��o para ligar o sensor
	
	teste = dataInputStream.read();  //recebendo a confirma��o se ligou 
	System.out.println("O sensor Portao enviou: "+teste);
	
	} catch (UnknownHostException e) {
	System.out.println("Nenhuma Conex�o Detectada1");
		e.printStackTrace();
		
	} catch (IOException e) {
		//teste=-1;
		System.out.println("Nenhuma Conex�o Detectada2");
		e.printStackTrace();
		return -1;
		
		
		
	} finally {
		
		try{
			if(socket!=null){
				socket.close();
			}
			if(dataOutputStream!=null){
				dataOutputStream.close();
			}
			if(dataInputStream!=null){
				dataInputStream.close();
			}
		} catch(Exception e){
			System.out.println("Nenhuma Conex�o Detectada3");
			e.printStackTrace();
		}
		
	}
	return teste;
}



static int conectaSensorTv(int ligado){
	int teste=-1;
	Socket socket = null;
    DataOutputStream dataOutputStream = null;
	DataInputStream dataInputStream = null; 
   
	try{
    
//	socket = new Socket("192.168.0.25",8084); // Tv
//	socket = new Socket("192.168.43.33",8084);
	socket = new Socket("192.168.43.204",8084);
		
	dataOutputStream = new DataOutputStream(socket.getOutputStream());//dado que ser� enviado ao server
	dataInputStream = new DataInputStream(socket.getInputStream());  //dado que ser� recebido do server
	
	
	dataOutputStream.write(ligado); //mandando a informa��o para ligar o sensor
	
	teste = dataInputStream.read();  //recebendo a confirma��o se ligou 
	System.out.println("O sensor TV enviou: "+teste);
	
	} catch (UnknownHostException e) {
	System.out.println("Nenhuma Conex�o Detectada1");
		e.printStackTrace();
		
	} catch (IOException e) {
		//teste=-1;
		System.out.println("Nenhuma Conex�o Detectada2");
		e.printStackTrace();
		return -1;
		
		
		
	} finally {
		
		try{
			if(socket!=null){
				socket.close();
			}
			if(dataOutputStream!=null){
				dataOutputStream.close();
			}
			if(dataInputStream!=null){
				dataInputStream.close();
			}
		} catch(Exception e){
			System.out.println("Nenhuma Conex�o Detectada3");
			e.printStackTrace();
		}
		
	}
	return teste;
}

static int conectaSensorPorta(int ligado){
	int teste=-1;
	Socket socket = null;
    DataOutputStream dataOutputStream = null;
	DataInputStream dataInputStream = null; 
   
	try{
    
//	socket = new Socket("192.168.0.25",8085); // Porta
//	socket = new Socket("192.168.43.33",8085);
	socket = new Socket("192.168.43.204",8085);
	
	dataOutputStream = new DataOutputStream(socket.getOutputStream());//dado que ser� enviado ao server
	dataInputStream = new DataInputStream(socket.getInputStream());  //dado que ser� recebido do server
	
	
	dataOutputStream.write(ligado); //mandando a informa��o para ligar o sensor
	
	teste = dataInputStream.read();  //recebendo a confirma��o se ligou 
	System.out.println("O sensor Porta enviou: "+teste);
	
	} catch (UnknownHostException e) {
	System.out.println("Nenhuma Conex�o Detectada1");
		e.printStackTrace();
		
	} catch (IOException e) {
		//teste=-1;
		System.out.println("Nenhuma Conex�o Detectada2");
		e.printStackTrace();
		return -1;
		
		
		
	} finally {
		
		try{
			if(socket!=null){
				socket.close();
			}
			if(dataOutputStream!=null){
				dataOutputStream.close();
			}
			if(dataInputStream!=null){
				dataInputStream.close();
			}
		} catch(Exception e){
			System.out.println("Nenhuma Conex�o Detectada3");
			e.printStackTrace();
		}
		
	}
	return teste;
}

static int conectaSensorLuminosidade(int ligado){
	int teste=-1;
	Socket socket = null;
    DataOutputStream dataOutputStream = null;
	DataInputStream dataInputStream = null; 
   
	try{
    
//	socket = new Socket("192.168.0.25",8085); // Luminosidade
//	socket = new Socket("192.168.43.33",8085);
//	socket = new Socket("192.168.43.20",8086);
	socket = new Socket("192.168.43.204",8086);
	
	dataOutputStream = new DataOutputStream(socket.getOutputStream());//dado que ser� enviado ao server
	dataInputStream = new DataInputStream(socket.getInputStream());  //dado que ser� recebido do server
	
	
	dataOutputStream.write(ligado); //mandando a informa��o para ligar o sensor
	
	teste = dataInputStream.read();  //recebendo a confirma��o se ligou 
	System.out.println("O sensor Luminosidade enviou: "+teste);
	
	} catch (UnknownHostException e) {
	System.out.println("Nenhuma Conex�o Detectada1");
		e.printStackTrace();
		
	} catch (IOException e) {
		//teste=-1;
		System.out.println("Nenhuma Conex�o Detectada2");
		e.printStackTrace();
		return -1;
		
		
		
	} finally {
		
		try{
			if(socket!=null){
				socket.close();
			}
			if(dataOutputStream!=null){
				dataOutputStream.close();
			}
			if(dataInputStream!=null){
				dataInputStream.close();
			}
		} catch(Exception e){
			System.out.println("Nenhuma Conex�o Detectada3");
			e.printStackTrace();
		}
		
	}
	return teste;
}

}
